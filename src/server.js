import cors from 'cors';
import express from "express";
import bodyParser from "body-parser";
import mongoose from "mongoose";

import routes from "./routes";

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(routes);

mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
    if (err) throw err;
    console.log("Conection succeful");
});

export { app as default }