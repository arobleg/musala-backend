import express from "express";

const app = express();

import gateway from "./gateway";
import peripheral from "./peripheral"

app.use(gateway);
app.use(peripheral);

export { app as default }