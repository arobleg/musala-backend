import express from "express";
import _ from "underscore";

const app = express();

import Peripheral from "../models/peripheral"
import Gateway from "../models/gateway"

app.get("/peripheral", async(req, res) => {
    const peripherals = await Peripheral
        .find()
        .populate("gateway", "name ip")
        .select("vendor status createAt")

    res.send(peripherals);
});

app.post("/peripheral", async(req, res) => {

    const { vendor, status, gateway } = _.pick(req.body, ["vendor", "status", "gateway"]);;

    const peripheral = new Peripheral({
        vendor,
        status,
        gateway
    });

    await peripheral.save((err, peripheralDB) => {

        if (!err) {

            Gateway.findOne({ _id: gateway }, (err, gatewayDB) => {
                gatewayDB.peripherals.push(peripheralDB);
                gatewayDB.save();
            });

            res.json({
                ok: true,
                peripheral: peripheralDB
            })

        } else {
            res.status(400).json({
                ok: false,
                err
            })
        }
    });
});

app.put("/peripheral/:id", function(req, res) {

    const id = req.params.id;
    const body = _.pick(req.body, ["vendor", "status", "gateway"]);;

    Peripheral.findByIdAndUpdate(id, body, { new: true }, (err, peripheralDB) => {

        if (err) {
            res.status(400).json({
                ok: false,
                err
            })
        }

        res.json({
            ok: true,
            peripheral: peripheralDB
        })
    })
});

app.delete("/peripheral/:id", function(req, res) {

    const id = req.query.id;

    Peripheral.findByIdAndRemove(id, (err, peripheralDelete) => {

        if (err) {
            res.status(400).json({
                ok: false,
                err
            })
        }
        res.json({
            ok: true,
            peripheral: peripheralDelete
        })

    })

});

export { app as default }