import express from "express";
import _ from "underscore";

import Gateway from "../models/gateway"
import Peripherals from "../models/peripheral"

const app = express();

app.get("/api/gateways", (req, res) => {

    const starting_at = Number(req.query.starting_at || 0);
    const limit = Number(req.query.limit || 10);

    Gateway.find({}, "name ip")
        .skip(starting_at)
        .limit(limit)
        .exec((err, gateways) => {

            if (err) {
                res.status(400).json({
                    ok: false,
                    err
                })
            }

            res.json({
                ok: true,
                gateways
            })

        });
});

app.post("/api/gateway", (req, res) => {

    let { name, ip } = _.pick(req.body, ["name", "ip"]);;

    let gateway = new Gateway({
        name,
        ip
    });

    gateway.save((err, gatewayDB) => {

        if (err) {
            res.status(400).json({
                ok: false,
                err
            })
        } else {
            res.json({
                ok: true,
                gateway: gatewayDB
            })
        }



    });
});

app.put("/api/gateway/:id", (req, res) => {

    const id = req.params.id;
    const body = _.pick(req.body, ["name", "ip"]);

    Gateway.findByIdAndUpdate(id, body, { new: true }, (err, gatewayDB) => {

        if (err) {
            res.status(400).json({
                ok: false,
                err
            })
        } else {
            res.json({
                ok: true,
                gateway: gatewayDB
            })
        }

    })
});

app.delete("/gateway/:id", (req, res) => {

    const id = req.query.id;

    Gateway.findByIdAndRemove(id, (err, gatewayDeleted) => {

        if (err) {
            res.status(400).json({
                ok: false,
                err
            })
        }
        res.json({
            ok: true,
            gateway: gatewayDeleted
        })

    })
});

export { app as default }