import '@babel/polyfill/noConflict'
import app from './server'

app.listen(process.env.PORT || 8000);