import mongoose from "mongoose";
import uniqueValidator from "mongoose-unique-validator";
import FlakeId from "flakeid";

const Schema = mongoose.Schema;

const peripheralSchema = new Schema({
    // UID (Unique Identifiers) not an UUID (Universally Unique Identifiers )
    _id: {
        type: Schema.Types.Number,
        default: () => {
            const flake = new FlakeId();
            return flake.gen();
        }
    },
    vendor: {
        type: String,
        trim: true,
        minlength: 3,
        required: [true, "Vendor is required"],
        trim: true
    },
    status: {
        type: String,
        required: [true, "Status is required"],
        enum: ["offline", "online"],
        lowercase: true,
        trim: true
    },
    createAt: {
        type: Date,
        default: Date.now
    },
    gateway: {
        type: Schema.Types.ObjectId,
        ref: "Gateway",
        required: [true, "Gateway is required"],
        validate: {
            message: "The Gateway can't be have more than 10 peripheral",
            validator: async(gateway_id) => {

                return await Peripheral.find({ gateway: gateway_id }).countDocuments() < 10;

            }
        }
    }
}, { collection: "peripheral" });

peripheralSchema.plugin(uniqueValidator);

const Peripheral = mongoose.model("Peripheral", peripheralSchema);

export { Peripheral as default }