import mongoose from "mongoose";
import uniqueValidator from "mongoose-unique-validator";

const Schema = mongoose.Schema;

const gatewaySchema = new Schema({
    name: {
        type: String,
        unique: true,
        trim: true,
        minlength: 3,
        required: [true, "Gateway name is required"]
    },
    ip: {
        type: String,
        unique: true,
        trim: true,
        required: [true, "Gateway ip is required"],
        match: /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
    }
}, { collection: "gateway" });

gatewaySchema.plugin(uniqueValidator);

const Gateway = mongoose.model("Gateway", gatewaySchema);

export { Gateway as default }