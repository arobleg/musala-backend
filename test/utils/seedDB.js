import MongoClient from "mongodb";
import FlakeId from "flakeid";

const seedDatabase = async() => {

    // Use connect method to connect to the server
    const client = await MongoClient.connect(process.env.MONGO_URI);

    const db = await client.db("test");
    const gateways = await db.collection("gateway");
    const peripherals = await db.collection("peripheral");

    const flake = new FlakeId();

    //Insert 1 Gateway and 4 Peripherals
    const gateway_1 = await gateways.insertOne({ name: "gateway_1", ip: "192.168.1.1" });

    for (let i = 0; i < 4; i++)
        await peripherals.insertOne({ _id: flake.gen(), vendor: `vendor_${i}`, status: "online", createdAt: Date.now, gateway: gateway_1.ops[0]._id });

    //Insert 1 Gateway and 10 Peripherals
    const gateway_2 = await gateways.insertOne({ name: "gateway_2", ip: "192.168.1.2" });

    for (let i = 0; i < 10; i++)
        await peripherals.insertOne({ _id: flake.gen(), vendor: `vendor_${i}`, status: "offline", createdAt: Date.now, gateway: gateway_2.ops[0]._id });

    await client.close();
};

export { seedDatabase as default }