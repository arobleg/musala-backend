import MongoClient from "mongodb";
import FlakeId from "flakeid";

const dropDatabase = async() => {

    // Use connect method to connect to the server
    const client = await MongoClient.connect(process.env.MONGO_URI);

    const db = await client.db("test");
    await db.dropDatabase();
    await client.close();
};

export { dropDatabase as default }