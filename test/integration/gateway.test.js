import request from "supertest";
import MongoClient from "mongodb";
import seedDatabase from "../utils/seedDB"
import dropDatabase from "../utils/dropDB"
import app from "../../src/server";

beforeEach(seedDatabase);
afterEach(dropDatabase);

describe("Gateways", () => {

    const invalid_data = [null, undefined, NaN, ""];
    const invalid_ip = [{ ip: "210.110", error: "must have 4 octets" },
        { ip: "255", case: "4 octets incomplete case" },
        { ip: "y.y.y.y", case: "only digits allowed case #1" },
        { ip: "255.0.0.y", case: "only digits allowed case #2" },
        { ip: "666.10.10.20", case: "digit must between [0-255] case #1" },
        { ip: "4444.11.11.11", case: "digit must between [0-255] case #2" },
        { ip: "33.3333.33.3", case: "digit must between [0-255] case #3" }
    ];

    describe("GET /api/gateways", () => {
        it("responds with json", (done) => {

            request(app)
                .get("/api/gateways")
                .expect((res) => {
                    expect(res.body.gateways.length).toBe(2)
                })
                .expect(200, done)
        });
    });

    describe("POST /api/gateway", () => {

        it("Rainbow road", (done) => {
            request(app)
                .post("/api/gateway")
                .send({ name: "gateway_3", ip: "192.168.5.1" })
                .expect((res) => {
                    expect(res.body.gateway).toMatchObject({ name: "gateway_3", ip: "192.168.5.1" })
                })
                .expect(200, done)
        });

        it("Ignoring unnecessary data", (done) => {
            request(app)
                .post("/api/gateway")
                .send({
                    name: "gateway_3",
                    ip: "192.168.5.1",
                    unnecessary: "unnecessary_1",
                    unnecessary_a: [
                        "a", "b", "c"
                    ]
                })
                .expect((res) => {
                    expect(res.body.gateway).toMatchObject({ name: "gateway_3", ip: "192.168.5.1" })
                })
                .expect(200, done)
        });

        it("Exist gateway name", (done) => {
            request(app)
                .post("/api/gateway")
                .send({ name: "gateway_1", ip: "192.168.1.10" })
                .expect(400, done)
        });

        it("Exist ip address name", (done) => {
            request(app)
                .post("/api/gateway")
                .send({ name: "gateway_10", ip: "192.168.1.1" })
                .expect(400, done)
        });

        invalid_data.forEach(data => {

            it(`Test invalid data case: name - ${data}`, (done) => {
                request(app)
                    .post("/api/gateway")
                    .send({ name: data, ip: "192.168.1.10" })
                    .expect(400, done)
            });

            it(`Test invalid data case: ip - ${data}`, (done) => {
                request(app)
                    .post("/api/gateway")
                    .send({ name: "gateway_10", ip: data })
                    .expect(400, done)
            });
        });

        invalid_ip.forEach(data => {
            it(`Test invalid ip case: name - ${data.case}`, (done) => {
                request(app)
                    .post("/api/gateway")
                    .send({ name: "gateway_3", ip: data.ip })
                    .expect(400, done)
            });

        })


    });

    describe("PUT /api/gateway/:id", () => {
        it("Update name - Rainbow road", async(done) => {

            // Use connect method to connect to the server
            const client = await MongoClient.connect(process.env.MONGO_URI);

            const db = await client.db("test");
            const gateway = await db.collection("gateway")
                .findOne();

            request(app)
                .put(`/api/gateway/${gateway._id}`)
                .send({ name: "gateway_3" })
                .expect((res) => {
                    expect(res.body.gateway).toMatchObject({ name: "gateway_3", ip: gateway.ip })
                })
                .expect(200, done);
        });

        it("Update ip - Rainbow road", async(done) => {

            // Use connect method to connect to the server
            const client = await MongoClient.connect(process.env.MONGO_URI);

            const db = await client.db("test");
            const gateway = await db.collection("gateway")
                .findOne();

            request(app)
                .put(`/api/gateway/${gateway._id}`)
                .send({ ip: "192.168.1.3" })
                .expect((res) => {
                    expect(res.body.gateway).toMatchObject({ name: gateway.name, ip: "192.168.1.3" })
                })
                .expect(200, done);
        });
    });
});